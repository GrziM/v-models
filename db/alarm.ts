export class AlarmState {
  deviceId: number;
  eventTypeId: number;
  eventTypeName: string;
  alarmOnState: number;
  inAlarmState: number;
  eventTime: string;
  timeStamp: string;
}

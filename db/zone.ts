export class Zone {
  zoneId: number;
  zoneName: string;
  deviceMode: number;
  polygon: string;
}

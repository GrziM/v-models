class AuthResult {
  userId: number;
  sessionTimeout: number;
}

class ProlongSessionResult {
  sessionTimeout: number;
}

class TokenResult {
  token: string;
}
